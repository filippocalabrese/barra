/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false,

            defaults: {
                template: 'Shinesoftware_Unicredit/payment/hosted',
            },

            /**
             * After place order callback
             */
            afterPlaceOrder: function () {
                // throw new Error("DEBUG - redirect the user to: " + window.checkoutConfig.payment.unicredit.redirectUrl);

                // redirection to the controller .../unicredit/hosted/redirect/
                $.mage.redirect(window.checkoutConfig.payment.unicredit_hosted.redirectUrl);
            },


            /** Returns payment acceptance mark link path */
            getPaymentAcceptanceMarkHref: function() {
                return window.checkoutConfig.payment.unicredit_hosted.paymentAcceptanceMarkHref;
            },

            /** Returns payment acceptance mark image path */
            getPaymentAcceptanceMarkSrc: function() {
                return window.checkoutConfig.payment.unicredit_hosted.paymentAcceptanceMarkSrc;
            },


        });
    }
);