<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shinesoftware\Unicredit\Gateway\Http;

use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use \Shinesoftware\Unicredit\Model\Logger\Shinelogger;

class TransferFactory implements TransferFactoryInterface
{
    /**
     * @var TransferBuilder
     */
    private $transferBuilder;

    /**
     * @var \Shinesoftware\Unicredit\Model\Logger\Shinelogger
     */
    public $_logger;

    /**
     * @param TransferBuilder $transferBuilder
     */
    public function __construct(
        TransferBuilder $transferBuilder,
        Shinelogger $logger

    )
    {
        $this->transferBuilder = $transferBuilder;
        $this->_logger = $logger;

    }

    /**
     * Builds gateway transfer object
     *
     * @param array $request
     * @return TransferInterface
     */
    public function create(array $request)
    {
        $config = array(
            'serverURL' => $request['serverURL'],
            'IgfsClass' => $request['IgfsClass']
        );

        $serverURL = $request['serverURL'];
        unset($request['IgfsClass']);
        unset($request['serverURL']);

        return $this->transferBuilder
            ->setClientConfig ($config)
            ->setBody ($request)
            ->setUri ($serverURL)
            ->build ();

    }
}
