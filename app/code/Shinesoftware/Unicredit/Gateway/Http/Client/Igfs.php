<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shinesoftware\Unicredit\Gateway\Http\Client;

require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/init/IgfsCgInit.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/init/IgfsCgSelector.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/init/IgfsCgVerify.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/init/InitTerminalInfo.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/tran/IgfsCgConfirm.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/tran/IgfsCgAuth.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/tran/IgfsCgCredit.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/tran/IgfsCgVoidAuth.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/mpi/IgfsCgMpiEnroll.php';
require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/mpi/IgfsCgMpiAuth.php';

use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;

class Igfs implements ClientInterface
{

    /**
     * @var Logger|\Shinesoftware\Unicredit\Model\Logger\Shinelogger
     */
    private $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(
        \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger
    )
    {
        $this->logger = $logger;
    }

    /**
     * Places request to gateway. Returns result as ENV array
     *
     * @param TransferInterface $transferObject
     * @return array
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $config = $transferObject->getClientConfig ();
        $data = array();

        $igfs = $this->callExternalClass ($config['IgfsClass']);
        $igfs->serverURL = $config['serverURL'];
        $igfs->disableCheckSSLCert ();

        try {
            foreach ($transferObject->getBody () as $key => $value) {
                $igfs->$key = $value;
            }
            $igfs->execute ();

            $data = [
                'request' => $transferObject->getBody (),
                'response' => $this->getResponse ($igfs)
            ];

            $this->logger->debug (__METHOD__ . " (" . __LINE__ . ") - URI: " . $config['serverURL']);
            $this->logger->debug (__METHOD__ . " (" . __LINE__ . ") - Init parameters: " . json_encode ($data['request']));
            $this->logger->debug (__METHOD__ . " (" . __LINE__ . ") - ".$config['IgfsClass']." Response: " . json_encode ($data['response']));

        }catch (\Exception $e){
            $this->logger->debug (__METHOD__ . " (" . __LINE__ . ") - ".$config['IgfsClass']." Error: " . $e->getMessage ());
        }


        return $data;
    }

    /**
     * @param $IgfsClassName
     * @return mixed
     * @throws \Exception
     */
    protected function callExternalClass($IgfsClassName)
    {

        if (class_exists ($IgfsClassName)) {
            return new $IgfsClassName;
        }

        throw new \Exception('Class IGFS has been not found!');
    }

    /**
     * Returns result code
     *
     * @param Igfs class
     * @return string
     */
    private function getResponse($igfs)
    {
        $data = json_decode(json_encode($igfs), true);

        return $data;
    }

}
