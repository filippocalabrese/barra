<?php
/**
 * @category  Apptrian
 * @package   Apptrian_FacebookCatalog
 * @author    Apptrian
 * @copyright Copyright (c) 2018 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
 
namespace Apptrian\FacebookCatalog\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    public $moduleList;
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;
    
    /**
     * @var \Magento\Framework\Filesystem
     */
    public $filesystem;
    
    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    public $driverFile;
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;
    
    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    public $configurableProductModel;
    
    /**
     * @var \Magento\Bundle\Model\Product\Type
     */
    public $bundleProductModel;
    
    /**
     * @var \Magento\GroupedProduct\Model\Product\Type\Grouped
     */
    public $groupedProductModel;
    
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    public $stockRegistry;
    
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    public $viewAssetRepo;
    
    /**
     * @var \Magento\Catalog\Helper\Data
     */
    public $catalogHelper;
    
    /**
     * Directory full path.
     *
     * @var null|string
     */
    public $directoryPath = null;
    
    /**
     * Store object
     *
     * @var null|\Magento\Store\Model\Store
     */
    public $store = null;
    
    /**
     * Store ID
     *
     * @var null|int
     */
    public $storeId = null;
    
    /**
     * Store URL
     *
     * @var null|string
     */
    public $storeUrl = null;
    
    /**
     * Store media URL
     *
     * @var null|string
     */
    public $storeMediaUrl = null;
    
    /**
     * Store name
     *
     * @var null|string
     */
    public $storeName = null;
    
    /**
     * Feed format
     *
     * @var null|string
     */
    public $feedFormat = null;
    
    /**
     * Allow products that are not visible individually in product feed
     *
     * @var null|int
     */
    public $pnviAllowed = null;
    
    /**
     * Feed filename
     *
     * @var null|string
     */
    public $filename = null;
    
    /**
     * Description attribute key
     *
     * @var null|string
     */
    public $descriptionAttr = null;
    
    /**
     * Base currency code
     *
     * @var null|string
     */
    public $baseCurrencyCode = null;
    
    /**
     * Current currency code
     *
     * @var null|string
     */
    public $currentCurrencyCode = null;
    
    /**
     * Brand attribute key
     *
     * @var null|string
     */
    public $brandAttribute = null;
    
    /**
     * Default brand
     *
     * @var null|string
     */
    public $defaultBrand = null;
    
    /**
     * Domain name
     *
     * @var null|string
     */
    public $domainName = null;
    
    /**
     * Tax display flag
     *
     * @var null|int
     */
    public $taxDisplayFlag = null;
    
    /**
     * Tax catalog flag
     *
     * @var null|int
     */
    public $taxCatalogFlag = null;
    
    /**
     * Sale Price field flag
     *
     * @var null|int
     */
    public $isSalePriceEnabled = null;
    
    /**
     * Item Group ID flag
     *
     * @var null|int
     */
    public $isItemGroupIdEnabled = null;
    
    /**
     * Item Group ID attribute key
     *
     * @var null|string
     */
    public $itemGroupIdAttribute = null;
    
    /**
     * GTIN flag
     *
     * @var null|int
     */
    public $isGtinEnabled = null;
    
    /**
     * GTIN attribute key
     *
     * @var null|string
     */
    public $gtinAttribute = null;
    
    /**
     * MPN flag
     *
     * @var null|int
     */
    public $isMpnEnabled = null;
    
    /**
     * MPN attribute key
     *
     * @var null|string
     */
    public $mpnAttribute = null;
    
    /**
     * Google Product Catalog flag
     *
     * @var null|int
     */
    public $isGpcEnabled = null;
    
    /**
     * Google Product Catalog attribute key
     *
     * @var null|string
     */
    public $gpcAttribute = null;
    
    /**
     * Parent product object
     *
     * @var null|\Magento\Catalog\Model\Product
     */
    public $parentProduct = null;
    
    /**
     * Parent product ID
     *
     * @var null|int
     */
    public $parentProductId = null;
    
    /**
     * File handle
     *
     * @var null|\Magento\Framework\Filesystem\File\Write
     */
    public $fileWrite = null;
    
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Store\Model\StoreManagerInterface
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filesystem\Driver\File $driverFile
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $cP
     * @param \Magento\Bundle\Model\Product\Type $bundleProduct
     * @param \Magento\GroupedProduct\Model\Product\Type\Grouped $groupedProduct
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockReg
     * @param \Magento\Framework\View\Asset\Repository $viewAssetRepo
     * @param \Magento\Catalog\Helper\Data $catalogHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Driver\File $driverFile,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $cP,
        \Magento\Bundle\Model\Product\Type $bundleProduct,
        \Magento\GroupedProduct\Model\Product\Type\Grouped $groupedProduct,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockReg,
        \Magento\Framework\View\Asset\Repository $viewAssetRepo,
        \Magento\Catalog\Helper\Data $catalogHelper
    ) {
        $this->scopeConfig              = $context->getScopeConfig();
        $this->logger                   = $context->getLogger();
        $this->moduleList               = $moduleList;
        $this->storeManager             = $storeManager;
        $this->filesystem               = $filesystem;
        $this->driverFile               = $driverFile;
        $this->productFactory           = $productFactory;
        $this->configurableProductModel = $cP;
        $this->bundleProductModel       = $bundleProduct;
        $this->groupedProductModel      = $groupedProduct;
        $this->stockRegistry            = $stockReg;
        $this->viewAssetRepo            = $viewAssetRepo;
        $this->catalogHelper            = $catalogHelper;
        
        parent::__construct($context);
    }
    
    /**
     * Returns extension version.
     *
     * @return string
     */
    public function getExtensionVersion()
    {
        $moduleCode = 'Apptrian_FacebookCatalog';
        $moduleInfo = $this->moduleList->getOne($moduleCode);
        return $moduleInfo['setup_version'];
    }
    
    /**
     * Based on provided configuration path and store code or ID returns
     * configuration value.
     *
     * @param string $configPath
     * @param string|int $scope
     * @return string
     */
    public function getConfig($configPath, $scope = 'default')
    {
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $scope
        );
    }
    
    /**
     * Returns feed format from config.
     *
     * @param int $storeId
     * @return string
     */
    public function getFeedFormat($storeId)
    {
        return $this->getConfig(
            'apptrian_facebookcatalog/general/format',
            $storeId
        );
    }
    
    /**
     * Returns full directory path to where generated feeds will reside.
     *
     * @return string
     */
    public function buildDirectoryPath()
    {
        $dir = $this->filesystem->getDirectoryRead(
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
        
        return $dir->getAbsolutePath();
    }
    
    /**
     * Based on config creates filename for the feed.
     *
     * @param int $storeId
     * @param string $feedFormat
     * @return string
     */
    public function buildFilename($storeId, $feedFormat)
    {
        $customFilename = $this->getConfig(
            'apptrian_facebookcatalog/general/filename',
            $storeId
        );
        
        if ($feedFormat == 'xml-rss') {
            $ext = 'xml';
        } else {
            // It is csv or tsv string
            $ext = $feedFormat;
        }
        
        if ($customFilename) {
            return $customFilename . '.'  . $ext;
        } else {
            return 'store-' . $storeId . '.'  . $ext;
        }
    }
    
    /**
     * Returns store base url.
     *
     * @param  \Magento\Store\Model\Store $store
     * @return string
     */
    public function buildStoreUrl($store)
    {
        return $this->cleanUrl(
            $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB)
        );
    }
    
    /**
     * Returns store media url.
     *
     * @param \Magento\Store\Model\Store $store
     * @return string
     */
    public function buildStoreMediaUrl($store)
    {
        return $this->cleanUrl(
            $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
        );
    }
    
    /**
     * Returns header part of the file.
     *
     * @return string
     */
    public function buildHeader()
    {
        $s = '';
        
        if ($this->feedFormat == 'xml-rss') {
            $s  = '<?xml version="1.0"?>';
            $s .= "\n";
            $s .= '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">';
            $s .= "\n<channel>\n";
            $s .= "<title>" . $this->storeName . "</title>\n";
            $s .= "<link>" . $this->storeUrl . "</link>\n";
            $s .= "<description>Facebook Catalog Product Feed of ";
            $s .= $this->storeName . " store.</description>\n";
        } else {
            $h = [
                'id',
                'title',
                'description',
                'link',
                'image_link',
                'availability',
                'condition',
                'brand',
                'price'
            ];
            
            if ($this->isSalePriceEnabled) {
                $h[] = 'sale_price';
            }
            
            if ($this->isItemGroupIdEnabled) {
                $h[] = 'item_group_id';
            }
            
            if ($this->isGtinEnabled) {
                $h[] = 'gtin';
            }
            
            if ($this->isMpnEnabled) {
                $h[] = 'mpn';
            }
            
            if ($this->isGpcEnabled) {
                $h[] = 'google_product_category';
            }
            
            if ($this->feedFormat == 'tsv') {
                // TSV format
                $s = implode("\t", $h);
            } else {
                // CSV format
                $s = implode(',', $h);
            }
        }
        
        return $s;
    }
    
    /**
     * Returns footer part of the file.
     *
     * @return string
     */
    public function buildFooter()
    {
        $s = '';
        
        if ($this->feedFormat == 'xml-rss') {
            $s .= "</channel>\n</rss>";
        }
        
        return $s;
    }
    
    /**
     * Returns product entry.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function buildProductEntry($product)
    {
        $entry = [];
        
        // Find and set parent product id and object
        $this->parentProduct   = null;
        $this->parentProductId = null;
        
        $this->parentProductId = $this->getParentProductId($product->getId());
        
        if ($this->parentProductId) {
            $productModel        = $this->productFactory->create();
            $this->parentProduct = $productModel->setStoreId($this->storeId)
                ->load($this->parentProductId);
        }
        
        // Order of keys is significant because of buildHeader() TSV format
        $entry['id']           = $this->getEntryId($product);
        $entry['title']        = $this->getEntryTitle($product);
        $entry['description']  = $this->getEntryDescription($product);
        $entry['link']         = $this->getEntryLink($product);
        $entry['image_link']   = $this->getEntryImageLink($product);
        $entry['availability'] = $this->getEntryAvailability($product);
        $entry['condition']    = $this->getEntryCondition($product);
        $entry['brand']        = $this->getEntryBrand($product);
        $entry['price']        = $this->getEntryPrice($product);
        
        if ($this->isSalePriceEnabled) {
            $entry['sale_price'] = $this->getEntryPrice($product, true);
        }
        
        if ($this->isItemGroupIdEnabled) {
            $entry['item_group_id'] = $this->getEntryItemGroupId();
        }
        
        if ($this->isGtinEnabled) {
            $entry['gtin'] = $this->getEntryGtin($product);
        }
        
        if ($this->isMpnEnabled) {
            $entry['mpn'] = $this->getEntryMpn($product);
        }
        
        if ($this->isGpcEnabled) {
            $entry['google_product_category'] = $this
                ->getEntryGoogleProductCategory($product);
        }
        
        return $this->convertEntryArrayToString($entry);
    }
    
    /**
     * Based on entry array and config returns string.
     *
     * @param array $entry
     * @return string
     */
    public function convertEntryArrayToString($entry)
    {
        $s = '';
        
        if ($this->feedFormat == 'xml-rss') {
            $s .= "<item>\n";
            
            foreach ($entry as $key => $value) {
                $s .= "<g:" . $key . ">" . $this->prepareForXml($value, $key)
                . "</g:" . $key . ">\n";
            }
            
            $s .= "<item>";
        } else {
            if ($this->feedFormat == 'tsv') {
                $delimiter = "\t";
            } else {
                $delimiter = ',';
            }
            
            foreach ($entry as $value) {
                $s .= '"' . str_replace('"', "'", $value) . '"' . $delimiter;
            }
            
            $s = trim($s, $delimiter);
        }
        
        return $s;
    }
    
    /**
     * Returns entry ID value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryId($product)
    {
        // Facebook limit for id is 100 characters
        return $this->limitText($this->cleanText($product->getSku()), 100);
    }
    
    /**
     * Returns entry title value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryTitle($product)
    {
        // Facebook limit for title is 100 characters
        return $this->limitText($this->cleanText($product->getName()), 100);
    }
    
    /**
     * Returns entry description value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryDescription($product)
    {
        $description = '';
        
        $description = $this->getDescriptionValue(
            $product,
            $this->descriptionAttr
        );
        
        // If user selected attribute is empty find alternative
        if ($description == '') {
            // Description > Short Description > Meta Description > Product Name
            $priority = [
                'description',
                'short_description',
                'meta_description',
                'name'
            ];
            
            foreach ($priority as $attr) {
                $description = $this->getDescriptionValue($product, $attr);
                
                if ($description != '') {
                    break;
                }
            }
        }
        
        // Facebook limit for description is 5000 characters
        return $this->limitText($this->cleanText($description), 5000);
    }
    
    /**
     * Returns description value based on attribute.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $attribute
     * @return string
     */
    public function getDescriptionValue($product, $attribute)
    {
        $value = '';
        
        switch ($attribute) {
            case 'description':
                $value = $product->getDescription();
                break;
            case 'meta_description':
                $value = $product->getMetaDescription();
                break;
            case 'short_description':
                $value = $product->getShortDescription();
                break;
            case 'name':
                $value = $product->getName();
                break;
            default:
                $value = '';
        }
        
        return $value;
    }
    
    /**
     * Returns entry link value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryLink($product)
    {
        $notVis = \Magento\Catalog\Model\Product\Visibility
            ::VISIBILITY_NOT_VISIBLE;
        
        if ($product->getVisibility() == $notVis) {
            if ($this->parentProductId && $this->parentProduct) {
                $url = $this->cleanUrl($this->parentProduct->getProductUrl());
            } else {
                $m = sprintf(
                    'Product ID: %d, SKU: %s, Name: %s ',
                    $product->getId(),
                    $product->getSku(),
                    $product->getName()
                );
                $m .= 'is not visible individually but has no parent product.';
                $this->logger->critical($m);
                
                $url = $this->cleanUrl($product->getProductUrl());
            }
        } else {
            $url = $this->cleanUrl($product->getProductUrl());
        }
        
        if (!$this->isValidUrl($url)) {
            $url = $this->storeUrl . $url;
        }
        
        return $url;
    }
    
    /**
     * Based on product ID returns parent product ID or null for no parent.
     *
     * @param int $productId
     * @return null|int
     */
    public function getParentProductId($productId)
    {
        $parentId = null;
        
        // Configurable
        $parentIds = $this->configurableProductModel
            ->getParentIdsByChild($productId);
        
        if (!empty($parentIds) > 0 && isset($parentIds[0])) {
            $parentId = $parentIds[0];
            return $parentId;
        }
        
        // Bundle
        $parentIds = $this->bundleProductModel
            ->getParentIdsByChild($productId);
        
        if (!empty($parentIds) > 0 && isset($parentIds[0])) {
            $parentId = $parentIds[0];
            return $parentId;
        }
        
        // Grouped
        $parentIds = $this->groupedProductModel
            ->getParentIdsByChild($productId);
        
        if (!empty($parentIds) > 0 && isset($parentIds[0])) {
            $parentId = $parentIds[0];
            return $parentId;
        }
        
        return $parentId;
    }
    
    /**
     * Returns entry image link value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryImageLink($product)
    {
        $imageUrl = null;
        $image = $product->getImage();
        
        if (!$image || $image === '' || $image === 'no_selection') {
            $product->load('media_gallery');
            $gallery = $product->getMediaGalleryImages();
            if ($gallery) {
                foreach ($gallery as $galleryImage) {
                    if ($galleryImage['url'] && $galleryImage['url'] !== '') {
                        $imageUrl = $galleryImage['url'];
                        break;
                    }
                }
            }
        }
        
        if (!$imageUrl && $image != '' && $image !== 'no_selection') {
            $imageUrl = $this->storeMediaUrl . 'catalog/product' . $image;
        }
        
        if ($this->isValidUrl($imageUrl)) {
            return $imageUrl;
        } else {
            // Placeholder image
            return $this->viewAssetRepo->getUrl(
                'Magento_Catalog::images/product/placeholder/image.jpg'
            );
        }
    }
    
    /**
     * Returns entry availability value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryAvailability($product)
    {
        $stock = $this->stockRegistry->getStockItem($product->getId());
        
        if ($stock->getIsInStock()) {
            return 'in stock';
        } else {
            return 'out of stock';
        }
    }
    
    /**
     * Returns entry condition value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryCondition($product)
    {
        $condition = null;
        // Check that 'condition' attribute exists and is not null or empty etc.
        if ($product->getData('condition')) {
            $condition = $product->getAttributeText('condition');
            if (!$condition) {
                $condition = $product->getData('condition');
            }
        }
        
        if ($condition
            && ($condition === 'new'
                || $condition === 'used'
                || $condition === 'refurbished'
            )
        ) {
            return $condition;
        } else {
            // Default condition is 'new'
            return 'new';
        }
    }
    
    /**
     * Returns entry price value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryPrice($product, $salePrice = false)
    {
        return $this->formatPrice(
            $this->getProductPrice(
                $product,
                $salePrice
            ),
            $this->currentCurrencyCode
        );
    }
    
    /**
     * Returns product price.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param bool $salePrice
     * @return string
     */
    public function getProductPrice($product, $salePrice = false)
    {
        $price = 0.0;
        
        $productType = $product->getTypeId();
        
        switch ($productType) {
            case 'bundle':
                $price =  $this->getBundleProductPrice($product, $salePrice);
                break;
            case 'configurable':
                $price = $this->getConfigurableProductPrice(
                    $product,
                    $salePrice
                );
                break;
            case 'grouped':
                $price = $this->getGroupedProductPrice($product, $salePrice);
                break;
            default:
                $price = $this->getFinalPrice($product, $salePrice);
        }
        
        return $price;
    }
    
    /**
     * Returns bundle product price.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param bool $salePrice
     * @return string
     */
    public function getBundleProductPrice($product, $salePrice = false)
    {
        if ($this->isSalePriceEnabled) {
            if ($salePrice) {
                $price = $product->getPriceInfo()->getPrice('final_price')
                    ->getMinimalPrice()->getValue();
            } else {
                $price = $product->getPriceInfo()->getPrice('regular_price')
                    ->getMinimalPrice()->getValue();
            }
        } else {
            $price = $product->getPriceInfo()->getPrice('final_price')
                ->getMinimalPrice()->getValue();
        }
        
        return $this->getFinalPrice($product, $salePrice, $price);
    }
    
    /**
     * Returns configurable product price.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param bool $salePrice
     * @return string
     */
    public function getConfigurableProductPrice($product, $salePrice = false)
    {
        if ($product->getFinalPrice() === 0) {
            $subCollection = $product->getTypeInstance()
                ->getUsedProducts($product);
            
            foreach ($subCollection as $subProduct) {
                if ($subProduct->getPrice() > 0) {
                    return $this->getFinalPrice($subProduct, $salePrice);
                }
            }
        }
        
        return $this->getFinalPrice($product, $salePrice);
    }
    
    /**
     * Returns grouped product price.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param bool $salePrice
     * @return string
     */
    public function getGroupedProductPrice($product, $salePrice = false)
    {
        $assocProducts = $product->getTypeInstance(true)
            ->getAssociatedProductCollection($product)
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('regular_price')
            ->addAttributeToSelect('special_price')
            ->addAttributeToSelect('tax_class_id')
            ->addAttributeToSelect('tax_percent');
        
        $prices = [];
        
        foreach ($assocProducts as $assocProduct) {
            $assocPrice = $this->getFinalPrice($assocProduct, $salePrice);
            
            if ($assocPrice) {
                $prices[] = $assocPrice;
            }
        }
        
        return min($prices);
    }
    
    /**
     * Returns final price.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $price
     * @return string
     */
    public function getFinalPrice($product, $salePrice = false, $price = null)
    {
        if ($price === null) {
            if ($this->isSalePriceEnabled) {
                if ($salePrice) {
                    $price = $product->getFinalPrice();
                    if ($price == 0) {
                        $price = $product->getPriceInfo()
                            ->getPrice('special_price')->getValue();
                    }
                    
                    $regularPrice = $product->getPriceInfo()
                        ->getPrice('regular_price')->getValue();
                    
                    if ($price == $regularPrice) {
                        $price = '';
                    }
                } else {
                    $price = $product->getPriceInfo()->getPrice('regular_price')
                        ->getValue();
                }
            } else {
                $price = $product->getFinalPrice();
            }
        }
        
        // 1. Convert to current currency if needed
        $price = $this->convertCurrency($price, $product->getTypeId());
        
        // 2. Apply tax if needed
        $price = $this->applyTax($price, $product);
        
        return $price;
    }
    
    /**
     * Converts price into current currency if needed.
     *
     * @param string|float $price
     * @param string $productType
     * @return string|float
     */
    public function convertCurrency($price, $productType)
    {
        // If there is no price
        if ($price === '') {
            return '';
        }
        
        // Convert price if base and current currency are not the same
        // Except for configurable products they already have currency converted
        if (($this->baseCurrencyCode !== $this->currentCurrencyCode)
            && $productType != 'configurable'
        ) {
            // Convert to from base currency to current currency
            $price = $this->store->getBaseCurrency()
                ->convert($price, $this->currentCurrencyCode);
        }
        
        return $price;
    }
    
    /**
     * Returns price with applied tax if needed.
     *
     * @param string|float $price
     * @param \Magento\Catalog\Model\Product $product
     * @return string|float
     */
    public function applyTax($price, $product)
    {
        // If there is no price
        if ($price === '') {
            return '';
        }
        
        $productType = $product->getTypeId();
        
        // Simple, Virtual, Downloadable products price is without tax
        // Grouped products have associated products without tax
        // Bundle products price already have tax included/excluded
        // Configurable products price already have tax included/excluded
        if ($productType != 'configurable' && $productType != 'bundle') {
            // If display tax flag is on and catalog tax flag is off
            if ($this->taxDisplayFlag && !$this->taxCatalogFlag) {
                $price = $this->catalogHelper->getTaxPrice(
                    $product,
                    $price,
                    true,
                    null,
                    null,
                    null,
                    $this->storeId,
                    false,
                    false
                );
            }
        }
        
        // Case when catalog prices are with tax but display tax is set to
        // to exclude tax. Applies for all products
        // If display tax flag is off and catalog tax flag is on
        if (!$this->taxDisplayFlag && $this->taxCatalogFlag) {
            $price = $this->catalogHelper->getTaxPrice(
                $product,
                $price,
                false,
                null,
                null,
                null,
                $this->storeId,
                true,
                false
            );
        }
        
        return $price;
    }
    
    /**
     * Returns formated price.
     *
     * @param string $price
     * @param string $currencyCode
     * @return string
     */
    public function formatPrice($price, $currencyCode = '')
    {
        if ($price == '' || $price == 0) {
            return '';
        }
        
        $formatedPrice = number_format($price, 2, '.', '');
        
        if ($currencyCode) {
            return $formatedPrice . ' ' . $currencyCode;
        } else {
            return $formatedPrice;
        }
    }
    
    /**
     * Returns entry brand value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryBrand($product)
    {
        $brand = '';
        
        // Check that 'brand' attribute exists and is not null or empty etc.
        if ($product->getData($this->brandAttribute)) {
            $brand = $product->getAttributeText($this->brandAttribute);
            if (!$brand) {
                $brand = $product->getData($this->brandAttribute);
            }
        }
        
        // If it is empty use default brand
        if ($brand == '') {
            $brand = $this->defaultBrand;
        }
        
        // If it is still empty use store name
        if ($brand == '') {
            $brand = $this->storeName;
        }
        
        // If it is still empty use domain name
        if ($brand == '') {
            $brand = $this->domainName;
        }
        
        // Facebook limit for brand/mpn/gtin is 70 characters
        return $this->limitText($this->cleanText($brand), 70);
    }
    
    /**
     * Returns domain name from the string.
     *
     * @param string $url
     * @return string
     */
    public function getDomainNameFromUrl($url)
    {
        // Remove http and https part from $url
        if (substr($url, 0, strlen('http://')) == 'http://') {
            $url = substr($url, strlen('http://'));
        }
        
        if (substr($url, 0, strlen('https://')) == 'https://') {
            $url = substr($url, strlen('https://'));
        }
        
        // Remove '/' sign
        return trim($url, '/');
    }
    
    /**
     * Returns item group id based on desired attribute (by default parent SKU).
     *
     * @return string
     */
    public function getEntryItemGroupId()
    {
        $itemGroupId = '';
        
        if ($this->parentProductId && $this->parentProduct) {
            if ($this->itemGroupIdAttribute) {
                if ($this->itemGroupIdAttribute == 'sku') {
                    $itemGroupId = $this->parentProduct->getSku();
                } else {
                    // Check that 'itemGroupId' attribute exists and is not null
                    // or empty etc.
                    if ($this->parentProduct
                            ->getData($this->itemGroupIdAttribute)
                    ) {
                        $itemGroupId = $this->parentProduct->getAttributeText(
                            $this->itemGroupIdAttribute
                        );
                        
                        if (!$itemGroupId) {
                            $itemGroupId = $this->parentProduct->getData(
                                $this->itemGroupIdAttribute
                            );
                        }
                    }
                }
            }
            
            // Facebook limit for id is 100 characters
            $itemGroupId = $this->limitText(
                $this->cleanText($itemGroupId),
                100
            );
        }
        
        return $itemGroupId;
    }
    
    /**
     * Returns entry GTIN value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryGtin($product)
    {
        $gtin = '';
        
        if ($this->gtinAttribute) {
            $gtin = $this->getProductGtin($product, $this->gtinAttribute);
        }
        
        // If selected attribute is empty provide alternative
        if ($gtin == '') {
            $priority = [
                'gtin',
                'upc',
                'ean',
                'isbn',
                'jan'
            ];
            
            foreach ($priority as $attr) {
                $gtin = $this->getProductGtin($product, $attr);
                if ($gtin) {
                    break;
                }
            }
        }
        
        // Facebook limit for gtin is 70 characters
        return $this->limitText($this->cleanText($gtin), 70);
    }
    
    /**
     * Returns gtin from product object based on attribute.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $attr
     * @return string
     */
    public function getProductGtin($product, $attr)
    {
        $gtin = '';
        
        // Check that attribute exists and is not null or empty etc.
        if ($product->getData($attr)) {
            $gtin = $product->getAttributeText($attr);
            if (!$gtin) {
                $gtin = $product->getData($attr);
            }
        }
        
        return $gtin;
    }
    
    /**
     * Returns entry MPN value from product object.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryMpn($product)
    {
        $mpn = '';
        
        // Check that 'mpn' attribute exists and is not null or empty etc.
        if ($this->mpnAttribute && $product->getData($this->mpnAttribute)) {
            $mpn = $product->getAttributeText($this->mpnAttribute);
            if (!$mpn) {
                $mpn = $product->getData($this->mpnAttribute);
            }
        }
        
        // Facebook limit for MPN is 70 characters
        return $this->limitText($this->cleanText($mpn), 70);
    }
    
    /**
     * Returns Google Product Category
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getEntryGoogleProductCategory($product)
    {
        $gpc = '';
        
        // Check that 'gpc' attribute exists and is not null or empty etc.
        if ($this->gpcAttribute && $product->getData($this->gpcAttribute)) {
            $gpc = $product->getAttributeText($this->gpcAttribute);
            if (!$gpc) {
                $gpc = $product->getData($this->gpcAttribute);
            }
        }
        
        // Facebook limit for Google Product Category is 250 characters
        return $this->limitText($this->cleanText($gpc), 250);
    }
    
    /**
     * Returns cleaned string.
     *
     * @param string $str
     * @return string
     */
    public function cleanText($str)
    {
        // Decode HTML entities
        // Strip tags
        // Remove tabs, new lines and replace them with one space
        // Truncate multiple spaces to one space
        // Remove white space chars on both sides
        return trim(
            preg_replace(
                "/ {2,}/",
                " ",
                str_replace(
                    ["\r\n", "\r", "\n", "\t"],
                    " ",
                    strip_tags(
                        html_entity_decode(
                            $str,
                            ENT_HTML5 | ENT_QUOTES,
                            'UTF-8'
                        )
                    )
                )
            )
        );
    }
    
    /**
     * Limits text if text is longer than $limit.
     *
     * @param string $string
     * @param integer $limit
     * @return string
     */
    public function limitText($string, $limit)
    {
        if (function_exists('mb_substr')) {
            $str = mb_substr($string, 0, $limit, 'UTF-8');
        } else {
            $str = substr($string, 0, $limit);
        }
        
        return $str;
    }
    
    /**
     * Returns cleaned url.
     *
     * @param string $url
     * @return string
     */
    public function cleanUrl($url)
    {
        $queryPosition = strpos($url, '?');
        
        // If there is a query string remove it
        if ($queryPosition !== false) {
            $url = substr($url, 0, $queryPosition);
        }
        
        return $url;
    }
    
    /**
     * Checks if URL is valid.
     *
     * @param unknown $url
     * @return boolean
     */
    public function isValidUrl($url)
    {
        if (substr($url, 0, 4) === 'http') {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Returns prepared text for XML.
     * ENT_HTML5 and ENT_XML1 only in PHP 5.4 and above.
     *
     * @param string $str
     * @param string $field
     * @return string
     */
    public function prepareForXml($str, $field)
    {
        $prepared = htmlspecialchars($str, ENT_XML1 | ENT_QUOTES, 'UTF-8');
        
        // Some fields need to be limited agin because above converts some chars
        // to entities &quot; &apos; etc.
        switch ($field) {
            case 'id':
                $prepared = $this->limitText($prepared, 100);
                break;
            case 'description':
                $prepared = $this->limitText($prepared, 5000);
                break;
            case 'title':
                $prepared = $this->limitText($prepared, 100);
                break;
            case 'brand':
                $prepared = $this->limitText($prepared, 70);
                break;
            case 'mpn':
                $prepared = $this->limitText($prepared, 70);
                break;
            case 'gtin':
                $prepared = $this->limitText($prepared, 70);
                break;
            default:
                // Already prepared before switch statement
        }
        
        return $prepared;
    }

    /**
     * Main method for generating product feed
     * generate() -> saveToFile() -> writeProducts()
     */
    public function generate()
    {
        $this->directoryPath = $this->buildDirectoryPath();

        $stores = $this->storeManager->getStores();
        
        foreach ($stores as $store) {
            $this->store         = $store;
            $this->storeId       = $store->getId();
            $this->storeUrl      = $this->buildStoreUrl($store);
            $this->storeMediaUrl = $this->buildStoreMediaUrl($store);
            $this->storeName     = $store->getFrontendName();
            $this->feedFormat    = $this->getFeedFormat($this->storeId);
            $this->filename      = $this->buildFilename(
                $this->storeId,
                $this->feedFormat
            );
            
            $this->pnviAllowed = $this->getConfig(
                'apptrian_facebookcatalog/general/pnvi_allowed',
                $this->storeId
            );
            
            $this->descriptionAttr = $this->getConfig(
                'apptrian_facebookcatalog/general/description_attribute',
                $this->storeId
            );
            
            $this->baseCurrencyCode = strtoupper(
                $store->getBaseCurrencyCode()
            );
            
            $this->currentCurrencyCode = strtoupper(
                $store->getCurrentCurrencyCode()
            );
            
            $this->brandAttribute = $this->getConfig(
                'apptrian_facebookcatalog/general/brand_attribute',
                $this->storeId
            );
            
            $this->defaultBrand = $this->getConfig(
                'apptrian_facebookcatalog/general/default_brand',
                $this->storeId
            );
            
            $this->domainName = $this->getDomainNameFromUrl($this->storeUrl);
            
            // "Stores > Cofiguration > Sales > Tax > Calculation Settings
            // > Catalog Prices" configuration value
            $this->taxCatalogFlag = (int) $this->getConfig(
                'tax/calculation/price_includes_tax',
                $this->storeId
            );
            
            // "Stores > Cofiguration > Sales > Tax > Price Display Settings
            // > Display Product Prices In Catalog" configuration value
            // Tax Display values
            // 1 - excluding tax
            // 2 - including tax
            // 3 - including and excluding tax
            $flag = (int) $this->getConfig(
                'tax/display/type',
                $this->storeId
            );
            
            // 0 means price excluding tax, 1 means price including tax
            if ($flag == 1) {
                $this->taxDisplayFlag = 0;
            } else {
                $this->taxDisplayFlag = 1;
            }
            
            $this->isSalePriceEnabled = $this->getConfig(
                'apptrian_facebookcatalog/general/sale_price_enabled',
                $this->storeId
            );
            
            $this->isItemGroupIdEnabled = $this->getConfig(
                'apptrian_facebookcatalog/general/item_group_id_enabled',
                $this->storeId
            );
            
            $this->itemGroupIdAttribute = $this->getConfig(
                'apptrian_facebookcatalog/general/item_group_id_attribute',
                $this->storeId
            );
            
            $this->isGtinEnabled = $this->getConfig(
                'apptrian_facebookcatalog/general/gtin_enabled',
                $this->storeId
            );
            
            $this->gtinAttribute = $this->getConfig(
                'apptrian_facebookcatalog/general/gtin_attribute',
                $this->storeId
            );
            
            $this->isMpnEnabled = $this->getConfig(
                'apptrian_facebookcatalog/general/mpn_enabled',
                $this->storeId
            );
            
            $this->mpnAttribute = $this->getConfig(
                'apptrian_facebookcatalog/general/mpn_attribute',
                $this->storeId
            );
            
            $gpc = 'google_product_category';
            
            $this->isGpcEnabled = $this->getConfig(
                'apptrian_facebookcatalog/general/' . $gpc . '_enabled',
                $this->storeId
            );
            
            $this->gpcAttribute = $this->getConfig(
                'apptrian_facebookcatalog/general/' . $gpc . '_attribute',
                $this->storeId
            );
            
            $this->saveToFile();
        }
    }

    /**
     * Save feed to a file.
     */
    public function saveToFile()
    {
        $dirWrite = $this->filesystem->getDirectoryWrite(
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
        
        // Check that path is writable
        if ($dirWrite->isExist() && !$dirWrite->isWritable()) {
            $this->logger->critical('Feed file is not writable.');
            $message = sprintf(
                __('Please make sure the path %s is writable by web server.'),
                $this->directoryPath
            );
            throw new \Exception($message);
        }
        
        // Delete previous feed file even ones with changed extension
        $formats = ['csv', 'tsv', 'xml-rss'];
        
        foreach ($formats as $format) {
            $filename = $this->buildFilename(
                $this->storeId,
                $format
            );
            
            if ($this->driverFile->isExists($this->directoryPath . $filename)) {
                $this->driverFile->deleteFile($this->directoryPath . $filename);
            }
        }
        
        // Open file
        $this->fileWrite = $dirWrite->openFile($this->filename, 'w');
        
        try {
            $this->fileWrite->lock();
            try {
                // Write header
                $this->fileWrite->write($this->buildHeader() . "\n");
                
                // Check how many products there will be
                $productModel = $this->productFactory->create();
                $collection = $productModel->getCollection()
                    ->addStoreFilter($this->storeId);
                $totalNumberOfProducts = $collection->getSize();
                unset($collection);
                unset($productModel);
                
                // Write products
                $this->writeProducts($totalNumberOfProducts, true);
                
                // Write footer
                $footer = $this->buildFooter();
                if ($footer) {
                    $this->fileWrite->write($footer . "\n");
                }
            } finally {
                $this->fileWrite->unlock();
            }
        } finally {
            $this->fileWrite->close();
        }
    }
    
    /**
     * Write products to file.
     *
     * @param int $totalNumberOfProducts
     * @param bool $log
     * @throws Exception
     */
    public function writeProducts($totalNumberOfProducts, $log)
    {
        $count = 0;
        $batch = 100;
        $skipCount = 0;
        $exceptionCount = 0;
        
        $timeLimit = (int) ini_get('max_execution_time');
        if ($timeLimit !== 0 && $timeLimit < 3600) {
            set_time_limit(3600);
        }
        
        $notVis = \Magento\Catalog\Model\Product\Visibility
            ::VISIBILITY_NOT_VISIBLE;
        
        $statusDisabled = \Magento\Catalog\Model\Product\Attribute\Source\Status
            ::STATUS_DISABLED;
          
        while ($count < $totalNumberOfProducts) {
            $productModel = $this->productFactory->create();
            $products = $productModel->getCollection()
                ->addAttributeToSelect('*')
                ->addStoreFilter($this->storeId)
                ->setPageSize($batch)
                ->setCurPage($count / $batch + 1)
                ->addUrlRewrite();
            
            foreach ($products as $product) {
                try {
                    $product->setStoreId($this->storeId)
                        ->load($product->getId());
                    $productName = $product->getName();
                    
                    if ($product->getStatus() != $statusDisabled
                        && $productName
                    ) {
                        if (!$this->pnviAllowed
                            && $product->getVisibility() == $notVis
                        ) {
                            $skipCount++;
                            continue;
                        }
                                
                        $entry = $this->buildProductEntry($product);
                                
                        $this->fileWrite->write($entry . "\n");
                    } else {
                        $skipCount++;
                    }
                } catch (\Exception $e) {
                    $exceptionCount++;
                    // Don't overload the logs, log the first 3 exceptions.
                    if ($exceptionCount <= 3) {
                        $this->logger->critical($e);
                    }
                    
                    // If it looks like a systemic failure stop feed generation
                    if ($exceptionCount > 100) {
                        throw $e;
                    }
                }
            }
            
            unset($products);
            unset($productModel);
            $count += $batch;
        }
        
        if ($skipCount != 0 && $log) {
            $this->logger->critical(sprintf('Skipped %d products', $skipCount));
        }
    }
    
    /**
     * Returns array of store product feed links data.
     *
     * @return array
     */
    public function getProductFeedLinks()
    {
        $data = [];
        
        $stores = $this->storeManager->getStores();
        
        foreach ($stores as $store) {
            $storeId       = $store->getId();
            $feedFormat    = $this->getFeedFormat($storeId);
            $feedFilename  = $this->buildFilename($storeId, $feedFormat);
            $storeMediaUrl = $this->buildStoreMediaUrl($store);
            
            $data[$storeId]['filename'] = $feedFilename;
            $data[$storeId]['name']     = $store->getName();
            $data[$storeId]['url']      = $storeMediaUrl . $feedFilename;
        }
        
        return $data;
    }
}
