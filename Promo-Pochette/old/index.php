<!doctype html>
<html lang="Default">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <link rel="shortcut icon" type="image/png" href="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/favicon.png">
    
	<link rel="stylesheet" type="text/css" href="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/style.css">
	<link rel="stylesheet" type="text/css" href="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/css/font-awesome.min.css">
	
    
	<script src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/js/jquery-2.1.0.js"></script>
	<script src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/js/bootstrap.js"></script>
	<script src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/js/blocs.js"></script>
	<script src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/js/lazysizes.min.js" defer></script>
    <title>Be Cool - Una pochette in regalo!</title>

    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>
<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc bgc-white l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<a href="https://www.lucabarra.it"><img src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/img/logo.png" class="center-block" width="260" /></a>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- Bloc Group -->
<div class='bloc-group'>

<!-- bloc-1 -->
<div class="bloc bgc-white bloc-tile-2 l-bloc b-divider " id="bloc-1">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-12">
				<img src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/img/lazyload-ph.png" data-src="https://www.inol3.com/servizio/lucabarra/Promo-Pochette/img/becool.png" class="center-block lazyload" width="210" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				
					<hr style="width:200px; border: 3px solid #000; margin-top:55px;">
					
				
			</div>
		</div>
		<div class="row">
				<div class="col-sm-12">
					<h1 class="text-center animDelay02  mg-clear tc-grullo">
						<strong>NON LASCIARTI SFUGGIRE <br>LA NOSTRA PROMO!!</strong><br>
					</h1>
					<h3 class=" text-w-70 text-center tc-black">
						Da sabato 15 settembre fino al 31 ottobre, acquistando 2 articoli Luca Barra presso i&nbsp;<a onclick="scrollToTarget('#bloc-5')"><strong><u>rivenditori aderenti</u></strong></a>&nbsp;per un valore minimo di 32 euro, riceverai in REGALO una pochette Luca Barra
					</h3>
				</div>
			</div>
	</div>
</div>
<!-- bloc-1 END -->

<!-- bloc-2 -->
<div class="bloc bgc-white bloc-tile-2 bg-pochette-8 d-bloc b-divider" id="bloc-2">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-12">
			</div>
		</div>
	</div>
</div>
<!-- bloc-2 END -->
</div>
<!-- Bloc Group END -->

<!-- bloc-3 -->
<div class="bloc bgc-white l-bloc none" id="bloc-3">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="text-center tc-black mg-sm">
					<i>Promozione valida da sabato 15 settembre salvo esaurimento scorte.</i>
				</h4>
				<h5 class="text-center tc-black mg-md">
					<i>Il regolamento è consultabile direttamente sul punto vendita aderente alla promozione.</i>
				</h5>
			</div>
		</div>
	</div>
</div>
<!-- bloc-3 END -->

<!-- bloc-4 -->
<div class="bloc bgc-black d-bloc" id="bloc-4">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="text-center tc-white mg-sm">
					<i>Scopri i punti vendita che aderiscono all&rsquo;iniziativa.</i>
				</h4>
			</div>
		</div>
	</div>
</div>
<!-- bloc-4 END -->

<!-- Footer - bloc-5 -->
<div class="bloc bgc-white full-width-bloc l-bloc" id="bloc-5">
	<div class="container">
		<div class="row row-no-gutters">
			<div class="col-sm-12">
				
					<iframe src="img/lazyload-ph.png" data-src="https://www.google.com/maps/d/u/0/embed?mid=1zSnSoGd9vBHuxJCtswsXlAOKqP1_tpiz" width="100%" height="640" class="lazyload"></iframe>
					
				
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-5 END -->

<!-- Footer - bloc-6 -->
<div class="bloc bgc-black d-bloc" id="bloc-6">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-5">
				<span class="empty-column x-thin"></span>
			</div>
			<div class="col-sm-1">
				<div class="text-center">
					<a class="social-lg" href="http://instagram.com/lucabarragioielli" target="_blank"><span class="fa fa-instagram icon-md"></span></a>
				</div>
			</div>
			<div class="col-sm-1">
				<div class="text-center">
					<a class="social-lg" href="http://www.facebook.com/lucabarra.it" target="_blank"><span class="fa fa-facebook-official icon-md"></span></a>
				</div>
			</div>
			<div class="col-sm-5">
				<span class="empty-column x-thin"></span>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-6 END -->

</div>
<!-- Main container END -->
    


<!-- Preloader -->
<div id="page-loading-blocs-notifaction"></div>
<!-- Preloader END -->

</body>
</html>
