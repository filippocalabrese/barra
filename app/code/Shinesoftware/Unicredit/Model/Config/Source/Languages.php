<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Used in creating options for Actions config value selection
 *
 */
namespace Shinesoftware\Unicredit\Model\Config\Source;

class Languages implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'ITA', 'label' => __('Italian')],
                ['value' => 'USA', 'label' => __('English')],
                ['value' => 'SLO', 'label' => __('Slovenian')],
                ['value' => 'SRB', 'label' => __('Serbian')],
                ['value' => 'FRA', 'label' => __('French')],
                ['value' => 'DEU', 'label' => __('German')],
                ['value' => 'ESP', 'label' => __('Spanish')],
               ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['ITA' => __('Italian'),
                'USA' => __('English'),
                'SLO' => __('Slovenian'),
                'SRB' => __('Serbian'),
                'FRA' => __('French'),
                'DEU' => __('German'),
                'ESP' => __('Spanish')
        ];
    }
}
