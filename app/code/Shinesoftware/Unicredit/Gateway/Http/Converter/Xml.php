<?php

namespace Shinesoftware\Unicredit\Gateway\Http\Converter;

use Magento\Payment\Gateway\Http\ConverterException;

class Xml implements \Magento\Payment\Gateway\Http\ConverterInterface
{
    protected $xml;

    /**
     * {@inheritdoc}
     */
    public function convert($source)
    {
        if($source->response->error){
            throw new ConverterException(__($source->response->errorDesc));
        }

        return [
            'paymentid' => $this->getPaymentId(),
            'hostedpageurl' => $this->getHostedPageURL() . "?paymentid=" . $this->getPaymentId(),
            'securitytoken' => $this->getToken()
        ];

    }

    /**
     * Get the error code from the request
     *
     * @return string
     */
    public function getErrorCode()
    {
        return (string)$this->xml->errorcode;
    }

    /**
     * Get the error message from the request
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return (string)$this->xml->errormessage;
    }

    /**
     * Parsing the xml replied from the Bank server
     *
     * @param string $xml
     * @return SimpleXMLElement
     */
    private function parserResponse($xml)
    {
        return simplexml_load_string($xml);
    }

    /**
     * Get PaymentID released from the bank server
     *
     * @return string
     */
    public function getPaymentId()
    {
        return (string)$this->xml->paymentid;
    }

    /**
     * Get the HostedpageUrl from the request to redirect the user to the success/failure page
     *
     * @return string
     */
    public function getHostedPageURL()
    {
        return (string)$this->xml->hostedpageurl;
    }

    /**
     * Get the security token
     *
     * @return string
     */
    public function getToken()
    {
        return (string)$this->xml->securitytoken;
    }

    /**
     * Check if there is an error
     *
     * @return bool
     */
    public function hasError()
    {
        return !empty((string)$this->xml->response) ? true : false;
    }


    /**
     * Convert credit cards xml tree to array
     *
     * @param \DOMXPath $xpath
     * @return array
     */
    protected function convertCreditCards(\DOMXPath $xpath)
    {
        $creditCards = [];
        /** @var \DOMNode $type */
        foreach ($xpath->query('/payment/credit_cards/type') as $type) {
            $typeArray = [];

            /** @var $typeSubNode \DOMNode */
            foreach ($type->childNodes as $typeSubNode) {
                switch ($typeSubNode->nodeName) {
                    case 'label':
                        $typeArray['name'] = $typeSubNode->nodeValue;
                        break;
                    default:
                        break;
                }
            }

            $typeAttributes = $type->attributes;
            $typeArray['order'] = $typeAttributes->getNamedItem('order')->nodeValue;
            $ccId = $typeAttributes->getNamedItem('id')->nodeValue;
            $creditCards[$ccId] = $typeArray;
        }
        uasort($creditCards, [$this, '_compareCcTypes']);
        $config = [];
        foreach ($creditCards as $code => $data) {
            $config[$code] = $data['name'];
        }
        return $config;
    }

    /**
     * Compare sort order of CC Types
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod) Used in callback.
     *
     * @param array $left
     * @param array $right
     * @return int
     */
    private function _compareCcTypes($left, $right)
    {
        return $left['order'] - $right['order'];
    }

    /**
     * Convert groups xml tree to array
     *
     * @param \DOMXPath $xpath
     * @return array
     */
    protected function convertGroups(\DOMXPath $xpath)
    {
        $config = [];
        /** @var \DOMNode $group */
        foreach ($xpath->query('/payment/groups/group') as $group) {
            $groupAttributes = $group->attributes;
            $id = $groupAttributes->getNamedItem('id')->nodeValue;

            /** @var $groupSubNode \DOMNode */
            foreach ($group->childNodes as $groupSubNode) {
                switch ($groupSubNode->nodeName) {
                    case 'label':
                        $config[$id] = $groupSubNode->nodeValue;
                        break;
                    default:
                        break;
                }
            }
        }
        return $config;
    }

    /**
     * Convert methods xml tree to array
     *
     * @param \DOMXPath $xpath
     * @return array
     */
    protected function convertMethods(\DOMXPath $xpath)
    {
        $config = [];
        /** @var \DOMNode $method */
        foreach ($xpath->query('/payment/methods/method') as $method) {
            $name = $method->attributes->getNamedItem('name')->nodeValue;
            /** @var $methodSubNode \DOMNode */
            foreach ($method->childNodes as $methodSubNode) {
                if ($methodSubNode->nodeType != XML_ELEMENT_NODE) {
                    continue;
                }
                $config[$name][$methodSubNode->nodeName] = $methodSubNode->nodeValue;
            }
        }
        return $config;
    }
}
