<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shinesoftware\Unicredit\Block\Masterpass;

class Error extends \Magento\Framework\View\Element\Template
{
    protected $_orderFactory;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->_orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct ($context);
    }

    /**
     * @param $content
     * @return \Magento\Framework\Phrase
     */
    public function __($content)
    {
        return __($content);
    }

}