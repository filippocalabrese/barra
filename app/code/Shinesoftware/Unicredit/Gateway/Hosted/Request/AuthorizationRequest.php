<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Gateway\Hosted\Request;

use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;

class AuthorizationRequest implements BuilderInterface
{

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var
     */
    public $_storeManager;

    /**
     * @var
     */
    public $_resolver;

    /**
     * @var \Shinesoftware\Unicredit\Model\Logger\Shinelogger
     */
    public $_logger;

    /**
     * InitializeRequest constructor.
     * @param \Magento\Payment\Gateway\ConfigInterface $config
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\Resolver $resolver
     * @param \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger
     */
    public function __construct(
        \Magento\Payment\Gateway\ConfigInterface $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\Resolver $resolver,
        \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger
    ) {
        $this->config = $config;
        $this->_storeManager = $storeManager;
        $this->_resolver = $resolver;
        $this->_logger = $logger;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject)
    {
        $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - Unicredit Authorization Request has started!");

        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var PaymentDataObjectInterface $payment */
        $payment = $buildSubject['payment'];
        $order = $payment->getOrder();
        $address = $order->getBillingAddress();
        $tid = $this->config->getValue('tid', $order->getStoreId());
        $ksig = $this->config->getValue('ksig', $order->getStoreId());
        $description = $this->config->getValue('description', $order->getStoreId());
        $trType = "authorize" == $this->config->getValue('payment_action', $order->getStoreId()) ? "AUTH" : "PURCHASE";

        // Get the server url endpoint
        if($this->config->getValue('debug', $order->getStoreId())){
            $url = $this->config->getValue('server_url_test', $order->getStoreId());
        }else {
            $url = $this->config->getValue('server_url_live', $order->getStoreId());
        }

        // prepare the request data
        $data = [
            'tid' => $tid,
            'kSig' => $ksig,
            'IgfsClass' => 'IgfsCgInit', // this parameter is to suggest the IGFS class at the TransferFactory class
            'shopUserName' => trim($address->getLastname()) . ", " . trim($address->getFirstname()),
            'shopUserRef' => $address->getEmail(),
            'trType' => $trType,
            'amount' => $order->getGrandTotalAmount()  * 100,
            'currencyCode' => $this->config->getValue('currency', $order->getStoreId()),
            'langID' => substr($this->_resolver->getLocale(), -2),
            'serverURL' => $url,
            'notifyURL' => $this->_storeManager->getStore()->getUrl('unicredit/hosted/notify', ['_current' => true]),
            'errorURL' => $this->_storeManager->getStore()->getUrl('unicredit/hosted/error', ['_current' => true]),
            'shopID' => $order->getOrderIncrementId(),
            'description' => !empty($description) ? $description : __('Products in my cart'),
            'addInfo1' => $order->getOrderIncrementId(),
            'freetext' => $this->config->getValue('freetext', $order->getStoreId()),
        ];

        return $data;

    }

}
