<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Controller\Mybank;

require_once BP . '/app/code/Shinesoftware/Unicredit/Model/Igfs/init/IgfsCgVerify.php';


class Notify extends \Shinesoftware\Unicredit\Controller\Main
{
    /**
     * Notification from the bank and Verify action
     *
     * @return void
     */
    public function execute()
    {
        $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');

        $this->_logger->debug (__METHOD__ . " (" . __LINE__ . ") - Bank has called the Notify Action!");
        $this->_logger->debug (__METHOD__ . " (" . __LINE__ . ") - Bank sent these params: " . json_encode($this->getRequest()->getParams()));

        $orderId = $this->_checkoutSession->getLastRealOrderId();

        if ($orderId) {
            $order = $this->getOrder($orderId);
            $parameters = $order->getPayment()->getAdditionalInformation('init');

            // Get the server url endpoint
            if($this->_dataHelper->getConfig ('payment/unicredit_mybank/debug')){
                $url = $this->_dataHelper->getConfig ('payment/unicredit_mybank/server_url_test');
            }else {
                $url = $this->_dataHelper->getConfig ('payment/unicredit_mybank/server_url_live');
            }

            // DO VERIFY!
            $verify = $this->_objectManager->get ('IgfsCgVerify');

            try {

                $verify->disableCheckSSLCert ();
                $verify->timeout = 150000;
                $verify->paymentID = $parameters['paymentID'];
                $verify->kSig = $parameters['kSig'];
                $verify->shopID = $parameters['shopID'];
                $verify->tid = $parameters['tid'];
                $verify->serverURL = $url;
                $verify->execute ();

                $this->_logger->debug (__METHOD__ . " (" . __LINE__ . ") - Verify result: " . json_encode ($verify, true));

                if ($verify->error) {
                    $this->_logger->debug (__METHOD__ . " (" . __LINE__ . ") - " . $verify->rc . ": " . $verify->errorDesc);
                    $this->messageManager->addNoticeMessage(__($verify->errorDesc));
                }

                $data = array(
                    'rc' => $verify->rc,
                    'errorDesc' => $verify->errorDesc,
                    'paymentID' => $parameters['paymentID'],
                    'tranID' => $verify->tranID,
                    'authCode' => $verify->authCode,
                    'enrStatus' => $verify->enrStatus,
                    'authStatus' => $verify->authStatus,
                    'maskedPan' => $verify->maskedPan,
                    'payInstrToken' => $verify->payInstrToken,
                    'expireMonth' => $verify->expireMonth,
                    'expireYear' => $verify->expireYear
                );

                $this->_createTransaction($order, $data, ("IGFS_000" == $verify->rc));

            }catch (\Exception $e){
                $this->messageManager->addNoticeMessage(__($e->getMessage ()));
            }

            if (!$verify->error) { // Completed
                $url = $storeManager->getStore()->getUrl('checkout/onepage/success', ['_current' => true]);
            }else {
                $this->_cancelPayment($verify->errorDesc);
                $url = $storeManager->getStore()->getUrl('checkout/cart/', ['_current' => true]);
            }

        }else{
            $url = $storeManager->getStore()->getUrl('checkout/cart/', ['_current' => true]);
            $this->messageManager->addNoticeMessage(__("Session expired! Please contact the store owner!"));

        }

        $this->_redirect($url);

    }

    /**
     * Get order object
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrder($orderId)
    {
        return $this->_orderFactory->create()->loadByIncrementId($orderId);
    }


}
