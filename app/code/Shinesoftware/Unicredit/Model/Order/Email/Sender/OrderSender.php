<?php
/**
 * Created by PhpStorm.
 * User: Shinesoftware
 * Date: 21/07/17
 * Time: 14:24
 */

namespace Shinesoftware\Unicredit\Model\Order\Email\Sender;

use Magento\Sales\Model\Order;

class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender {

    public function send(Order $order, $forceSyncMode = false)
    {
        $payment = $order->getPayment()->getMethodInstance()->getCode();

        if($this->globalConfig->getValue('payment/unicredit_settings/sendorderemail') &&
            strpos($payment, 'unicredit') !== false &&
            $forceSyncMode == false
        ){
            return false;
        }

        $order->setSendEmail(true);

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            if ($this->checkAndSend($order)) {
                $order->setEmailSent(true);
                $this->orderResource->saveAttribute($order, ['send_email', 'email_sent']);
                return true;
            }
        }

        $this->orderResource->saveAttribute($order, 'send_email');

        return false;
    }
}