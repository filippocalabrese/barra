/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false,

            defaults: {
                template: 'Shinesoftware_Unicredit/payment/mybank',
            },

            /**
             * Get payment method type.
             */
            getTitle: function () {
                console.log(this);

                return this.item.title;
            },

            /**
             * After place order callback
             */
            afterPlaceOrder: function () {
                // throw new Error("DEBUG - redirect the user to: " + window.checkoutConfig.payment.unicredit.redirectUrl);

                // redirection to the controller .../unicredit/mybank/redirect/
                $.mage.redirect(window.checkoutConfig.payment.unicredit_mybank.redirectUrl);
            },

            /** Returns payment acceptance mark link path */
            getPaymentAcceptanceMarkHref: function() {
                return window.checkoutConfig.payment.unicredit_mybank.paymentAcceptanceMarkHref;
            },

            /** Returns payment acceptance mark image path */
            getPaymentAcceptanceMarkSrc: function() {
                return window.checkoutConfig.payment.unicredit_mybank.paymentAcceptanceMarkSrc;
            },

        });
    }
);