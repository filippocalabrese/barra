<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shinesoftware\Unicredit\Block\Hosted;

class Redirect extends \Magento\Framework\View\Element\Template
{
    protected $_orderFactory;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->_orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct ($context);
    }

    /**
     * @param $content
     * @return \Magento\Framework\Phrase
     */
    public function __($content)
    {
        return __($content);
    }

    /**
     * Get the secure bank URL
     */
    public function getSecureUrl()
    {

        // Check if the customer calls the redirection page directly
        if(!$this->_checkoutSession->getLastRealOrder()->getId()){
            return false;
        }

        // Get the redirection data
        //$data = $this->_checkoutSession->getLastRealOrder()->getPayment()->getAdditionalInformation('init');
        $data = $this->_checkoutSession->getLastRealOrder()->getPayment()->getAdditionalInformation();

        //print_r($data);
        // ... maybe the user goes back with the browser buttons
        // and the redirectURL is expired and deleted from the
        // additional information in the payment object
        if(empty($data['redirectURL'])){
            return false;
        }

        $redirectURL = $data['redirectURL'];

        // Clear the redirection url for security reasons
        unset($data['redirectURL']);

        // Update the payment information
        $this->_checkoutSession->getLastRealOrder()->getPayment()->setAdditionalInformation('init', $data)->save();

        return $redirectURL;
    }

}