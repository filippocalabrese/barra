<?php

namespace Shinesoftware\Unicredit\Controller\Mybank;

class Error extends \Shinesoftware\Unicredit\Controller\Main
{

    /**
     * When a customer cancel payment from unicredit gateway.
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_cancelPayment();

        $message = "There was a problem in the payment process, please try again!";

        $this->messageManager->addErrorMessage($message);
        $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - " . $message);
        $this->_redirect($this->_url->getUrl('checkout/cart/index'));
    }

}
