<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Gateway\Hosted\Request;

use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;

class VoidRequest implements BuilderInterface
{

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var
     */
    public $_storeManager;

    /**
     * @var
     */
    public $_resolver;

    /**
     * @var \Shinesoftware\Unicredit\Model\Logger\Shinelogger
     */
    public $_logger;

    /**
     * InitializeRequest constructor.
     * @param \Magento\Payment\Gateway\ConfigInterface $config
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\Resolver $resolver
     * @param \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger
     */
    public function __construct(
        \Magento\Payment\Gateway\ConfigInterface $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\Resolver $resolver,
        \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger
    ) {
        $this->config = $config;
        $this->_storeManager = $storeManager;
        $this->_resolver = $resolver;
        $this->_logger = $logger;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject)
    {
        $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - Unicredit Void Request has started!");

        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var PaymentDataObjectInterface $payment */
        $paymentDataObj = $buildSubject['payment'];
        $authTransaction = $paymentDataObj->getPayment()->getAdditionalInformation('init');
        $order = $paymentDataObj->getOrder();
        $amount = $order->getGrandTotalAmount();

        // Get the server url endpoint
        if($this->config->getValue('debug', $order->getStoreId())){
            $url = $this->config->getValue('server_url_test', $order->getStoreId());
        }else {
            $url = $this->config->getValue('server_url_live', $order->getStoreId());
        }

        // prepare the request data
        $data = [
            'tid' => $this->config->getValue('tid', $order->getStoreId()),
            'kSig' => $this->config->getValue('ksig', $order->getStoreId()),
            'IgfsClass' => 'IgfsCgVoidAuth', // this parameter is to suggest the IGFS class at the TransferFactory class
            'serverURL' => $url, // this parameter is to suggest the IGFS class at the TransferFactory class
            'amount' => $amount * 100,
            'shopID' => $order->getOrderIncrementId(),
            'refTranID' => $authTransaction['tranID'],
        ];

        return $data;

    }

}
