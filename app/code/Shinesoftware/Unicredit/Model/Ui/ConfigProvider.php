<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\View\Asset\Repository as Asset;

/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{
    const METHOD_HOSTED = 'unicredit_hosted';
    const METHOD_MYBANK = 'unicredit_mybank';
    const METHOD_MASTERPASS = 'unicredit_masterpass';

    /**
     * @var string[]
     */
    protected $methodCodes = [
        self::METHOD_HOSTED,
        self::METHOD_MYBANK,
        self::METHOD_MASTERPASS
    ];

    /**
     * @var PaymentHelper
     */
    protected $paymentHelper;

    /**
     * @var \Magento\Payment\Model\Method\AbstractMethod[]
     */
    protected $methods = [];

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepo;

    /**
     * @param PaymentHelper $paymentHelper
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        UrlInterface $urlBuilder,
        Asset $assetRepo
    ) {
        $this->paymentHelper = $paymentHelper;
        $this->urlBuilder = $urlBuilder;
        $this->_assetRepo = $assetRepo;

        foreach ($this->methodCodes as $code) {
            $this->methods[$code] = $this->paymentHelper->getMethodInstance($code);
        }

    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {

        return [
            'payment' => [
                'unicredit_hosted' => [
                    'paymentAcceptanceMarkHref' => $this->getPaymentMarkWhatIsUnicreditUrl(),
                    'paymentAcceptanceMarkSrc' => $this->getHostedPaymentMarkImageUrl(),
                    'redirectUrl' => $this->urlBuilder->getUrl ('unicredit/hosted/redirect', ['_secure' => true]),
                ],
                'unicredit_mybank' => [
                    'paymentAcceptanceMarkHref' => $this->getPaymentMarkWhatIsUnicreditUrl(),
                    'paymentAcceptanceMarkSrc' => $this->getMyBankPaymentMarkImageUrl (),
                    'redirectUrl' => $this->urlBuilder->getUrl ('unicredit/mybank/redirect', ['_secure' => true]),
                ],
                'unicredit_masterpass' => [
                    'paymentAcceptanceMarkHref' => $this->getPaymentMarkWhatIsUnicreditUrl(),
                    'paymentAcceptanceMarkSrc' => $this->getMasterpassPaymentMarkImageUrl (),
                    'redirectUrl' => $this->urlBuilder->getUrl ('unicredit/masterpass/redirect', ['_secure' => true]),
                ]
            ]
        ];
    }

    /**
     * Get Unicredit "mark" image URL
     * Supposed to be used on payment methods selection
     * $staticSize is applicable for static images only
     *
     * @return string
     */
    public function getHostedPaymentMarkImageUrl()
    {
        return $this->_assetRepo->getUrl("Shinesoftware_Unicredit::images/unicredit_hosted_allinone.png"); //"https://www.unicreditgroup.eu/etc/designs/unicreditgroupn/img/static/logoHiRes.png";
    }

    /**
     * Get Unicredit "mark" image URL
     * Supposed to be used on payment methods selection
     * $staticSize is applicable for static images only
     *
     * @return string
     */
    public function getMasterpassPaymentMarkImageUrl()
    {
        return $this->_assetRepo->getUrl("Shinesoftware_Unicredit::images/unicredit_masterpass_allinone.png"); //"https://www.unicreditgroup.eu/etc/designs/unicreditgroupn/img/static/logoHiRes.png";
    }

    /**
     * Get Unicredit "mark" image URL
     * Supposed to be used on payment methods selection
     * $staticSize is applicable for static images only
     *
     * @return string
     */
    public function getMyBankPaymentMarkImageUrl()
    {
        return $this->_assetRepo->getUrl("Shinesoftware_Unicredit::images/unicredit_mybank_allinone.png"); //"https://www.unicreditgroup.eu/etc/designs/unicreditgroupn/img/static/logoHiRes.png";
    }

    /**
     * Get "What Is Unicredit" localized URL
     * Supposed to be used with "mark" as popup window
     *
     * @return string
     */
    public function getPaymentMarkWhatIsUnicreditUrl()
    {
        return "https://www.unicredit.it/";
    }

}
