/**
 * Copyright © Shine Software, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
], function (Component, rendererList) {
    'use strict';

    rendererList.push(
        {
            type: 'unicredit_hosted',
            component: 'Shinesoftware_Unicredit/js/view/payment/method-renderer/unicredit_hosted'
        },
        {
            type: 'unicredit_mybank',
            component: 'Shinesoftware_Unicredit/js/view/payment/method-renderer/unicredit_mybank'
        },
        {
            type: 'unicredit_masterpass',
            component: 'Shinesoftware_Unicredit/js/view/payment/method-renderer/unicredit_masterpass'
        }
    );

    /**
     * Add view logic here if needed
     **/
    return Component.extend({});
});
