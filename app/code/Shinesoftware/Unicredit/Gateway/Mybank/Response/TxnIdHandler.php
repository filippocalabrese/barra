<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Gateway\Mybank\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;

class TxnIdHandler implements HandlerInterface
{
    const TRANID = 'tranID';

    /**
     * Handles transaction id
     *
     * @param array $handlingSubject
     * @param array $response
     * @return void
     */
    public function handle(array $handlingSubject, array $response)
    {
        if (!isset($handlingSubject['payment'])
            || !$handlingSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        $paymentInfo = $handlingSubject['payment'];

        $payment = $paymentInfo->getPayment();

        $data = [
            'error' => $response['response']['error'],
            'errorDesc' => $response['response']['errorDesc'],
            'rc' => $response['response']['rc'],
            'tranID' => $response['response']['tranID'],
            'addInfo1' => $response['response']['addInfo1'],
            'addInfo2' => $response['response']['addInfo2'],
            'addInfo3' => $response['response']['addInfo3'],
            'addInfo4' => $response['response']['addInfo4'],
            'addInfo5' => $response['response']['addInfo5'],
            'pendingAmount' => $response['response']['pendingAmount']
        ];


        /** @var $payment \Magento\Sales\Model\Order\Payment */
        $payment->setTransactionId($response['response'][self::TRANID]);
        $payment->setIsTransactionClosed(false);
        $payment->setAdditionalInformation('capture', $response['response']);
        $payment->setTransactionAdditionalInfo('raw_details_info', $data);
    }
}
