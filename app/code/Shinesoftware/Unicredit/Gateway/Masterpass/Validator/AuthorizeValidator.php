<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Gateway\Masterpass\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;
use Shinesoftware\Unicredit\Gateway\Masterpass\Http\Client\Engine;

class AuthorizeValidator extends AbstractValidator
{
    const RESULT_CODE = 'rc';
    const PAYMENT_ID = 'paymentID';
    const REDIRECT_URL = 'redirectURL';

    /**
     * Performs validation of result code
     *
     * @param array $validationSubject
     * @return ResultInterface
     */
    public function validate(array $validationSubject)
    {
        if (!isset($validationSubject['response']) || !is_array($validationSubject['response'])) {
            throw new \InvalidArgumentException('Response does not exist');
        }

        $response = $validationSubject['response'];

        if ($this->isSuccessfulTransaction($response)) {
            return $this->createResult(
                true,
                []
            );
        } else {
            return $this->createResult(
                false,
                [__($response['response']['errorDesc'])]
            );
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isSuccessfulTransaction(array $data)
    {
        return
            !empty($data['response'][self::RESULT_CODE]) && $data['response'][self::RESULT_CODE] == "IGFS_000" &&
            !empty($data['response'][self::PAYMENT_ID]) &&
            !empty($data['response'][self::REDIRECT_URL]);
    }
}
