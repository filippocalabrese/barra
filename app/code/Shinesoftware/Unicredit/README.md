## Introduzione
Il modulo Unicredit Advanced è nato con lo scopo di comprendere tutte le funzionalità offerte dal gruppo Unicredit Banca. Il modulo di Unicredit offre la possibilità di connettere il proprio sito di commercio elettronico con il gruppo bancario Unicredit in modalità IGFS offrendo alla clientela la possibilità di pagare con carta di credito e nel frattempo ricevere il pagamento nel proprio conto Unicredit.

## Pagamenti Supportati

- VISA
- MASTERCARD
- VISA ELECTRON
- JBc
- MyBank
- Masterpass

### Compatibilità e Requisiti minimi

Versione: Community dalla release 1.9.x 

Estensioni richieste nel server:

- simplexml
- mcrypt
- hash
- GD
- DOM
- iconv
- curl
- SOAP (per Webservices API)

## Installazione

Seguire la guida presente al link https://docs.unicredit.Shinesoftware.it