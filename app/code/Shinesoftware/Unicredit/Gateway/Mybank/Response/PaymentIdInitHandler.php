<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Gateway\Mybank\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;

class PaymentIdInitHandler implements HandlerInterface
{
    const PAYMENT_ID = 'paymentID';
    const REDIRECT_URL = 'redirectURL';

    /**
     * Handles transaction id
     *
     * @param array $handlingSubject
     * @param array $response
     * @return void
     */
    public function handle(array $handlingSubject, array $result)
    {
        if (!isset($handlingSubject['payment'])
            || !$handlingSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        $paymentInfo = $handlingSubject['payment'];

        $payment = $paymentInfo->getPayment();

        /** @var $payment \Magento\Sales\Model\Order\Payment */
        $payment->setTransactionId($result['response'][self::PAYMENT_ID]);
        $payment->setIsTransactionClosed(false); // If you want to void an authorization, you have to prevent transaction from being closed. https://stackoverflow.com/a/10137390/1034359
        $payment->setAdditionalInformation('init', $result['response']);
        $payment->setTransactionAdditionalInfo('raw_details_info', $result['response']);
    }
}
