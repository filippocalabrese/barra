<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Model\Config\Source;

/**
 * Source model for available payment actions
 */
class Addinfo implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Addinfo Options
     */
    const ADDINFO_NULL = null;
    const ADDINFO_ORDERID = 'orderid';
    const ADDINFO_SHIPPING = 'shipping';
    const ADDINFO_SHIPPING_DESCRIPTION = 'shippingdescription';

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::ADDINFO_NULL,
                'label' => __('Please select...')
            ],[
                'value' => self::ADDINFO_ORDERID,
                'label' => __('Order ID')
            ],[
                'value' => self::ADDINFO_SHIPPING,
                'label' => __('Shipping')
            ],[
                'value' => self::ADDINFO_SHIPPING_DESCRIPTION,
                'label' => __('Shipping Description')
            ]
        ];
    }

}
