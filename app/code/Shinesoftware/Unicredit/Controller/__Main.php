<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Controller;

/**
 * Unicredit Checkout Controller
 */
abstract class Main extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Shinesoftware\Unicredit\Model\Logger\Shinelogger
     */
    protected $_logger;

    /**
     * @var \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface
     */
    protected $_transactionBuilder;

    /**
     * @var \Shinesoftware\Unicredit\Helper\Checkout
     */
    protected $_checkoutHelper;

    /**
     * @var \Magento\Sales\Api\Data\OrderStatusHistoryInterface
     */
    protected $_comment;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender
     */
    protected $_sender;

    /**
     * @var \Shinesoftware\Unicredit\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * Main constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $sender
     * @param \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger
     * @param \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $builder
     * @param \Shinesoftware\Unicredit\Helper\Checkout $checkout
     * @param \Shinesoftware\Unicredit\Helper\Data $data
     * @param \Magento\Sales\Api\Data\OrderStatusHistoryInterface $comment
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $sender,
        \Shinesoftware\Unicredit\Model\Logger\Shinelogger $logger,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $builder,
        \Shinesoftware\Unicredit\Helper\Checkout $checkout,
        \Shinesoftware\Unicredit\Helper\Data $data,
        \Magento\Sales\Api\Data\OrderStatusHistoryInterface $comment,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_checkoutHelper = $checkout;
        $this->_dataHelper = $data;
        $this->_sender = $sender;
        $this->_logger = $logger;
        $this->_transactionBuilder = $builder;
        $this->_comment = $comment;
        $this->_checkoutSession = $checkoutSession;
        $this->_pageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    /**
     * Cancel order, return quote to customer
     *
     * @param string $errorMsg
     * @return false|string
     */
    protected function _cancelPayment($errorMsg = '')
    {
        $errorMsg = trim(strip_tags($errorMsg));
        $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - Bank Response: " . $errorMsg);

        $this->_checkoutHelper->cancelCurrentOrder($errorMsg);
        if ($this->_checkoutSession->restoreQuote()) {
            $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - Quote has been restored! The user will be redirect to the cart!");
            return true;
        }else{
            $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - Quote has been NOT restored! The user will be redirected to the failure page!");
            return false;
        }

    }

    /**
     * @param null $order
     * @param array $paymentData
     * @return mixed
     */
    protected function _createTransaction($order = null, $paymentData = array(), $success = true)
    {
        try {
            //get payment object from order object
            $payment = $order->getPayment();
            $payment->setLastTransId($paymentData['paymentID']);
            $payment->setTransactionId($paymentData['paymentID']);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
            );

            //get the object of builder class
            $trans = $this->_transactionBuilder;

            $order_transaction = $trans
                ->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentData['paymentID'] . "-order")
                ->setFailSafe(true)
                //build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_ORDER);

            $order_transaction->save()->getTransactionId();

            $auth_transaction = $trans
                ->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentData['paymentID'])
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                //build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);

            $payment->setParentTransactionId($paymentData['paymentID'] . "-order");
            $payment->save();

            if ($success && $this->_sender->send($order, true)) { //Email will be sent immediately if forceSyncMode is true
                $order->setEmailSent(true);
            }

            $order->setState("processing")->setStatus("processing");
            $order->setTotalPaid($order->getGrandTotal());
            $order->save();

            return $auth_transaction->save()->getTransactionId();
        } catch (\Exception $e) {
            $this->_logger->error(__METHOD__ . " (" . __LINE__ . ") - " . $e->getMessage ());
        }
    }


    /**
     * In this function we have to retrieve
     * the Quote Id previously created in the Observer method
     *
     * @see Shinesoftware_Unicredit_Model_Observer
     * @return mixed
     */
    protected function _getApiQuoteId()
    {
        $quoteId = $this->_checkoutSession->getData('apiQuoteId');
        return $quoteId;
    }

    /**
     * In this function we have to retrieve
     * the Order Id previously created in the Observer method
     *
     * @see Shinesoftware_Unicredit_Model_Observer
     * @return mixed
     */
    protected function _getApiOrderId()
    {
        $orderId = $this->_checkoutSession->getData('apiOrderId');
        return $orderId;
    }
}
