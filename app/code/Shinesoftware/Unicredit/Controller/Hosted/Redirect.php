<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Unicredit\Controller\Hosted;

class Redirect extends \Shinesoftware\Unicredit\Controller\Main
{

    /**
     * Redirect to Hosted gateway at bank website
     *
     * @return void
     */
    public function execute()
    {
        $this->_logger->debug(__METHOD__ . " (" . __LINE__ . ") - User has been redirected to the secure bank website!");
        return $this->_pageFactory->create();
    }

}
